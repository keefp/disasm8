# PDP8 Disassembler

This is a disaaembler for PDP8 binaries.

It expects two parameters:

-parm - a yaml parameter file containing labels and comments which will be inserted into the output listing.
-binary - source binary file.

Using the test source 4k.bin and the test parameter file 4k.bin:

go run cmd/disasm.go -parm test.yaml -binary 4k.bin

Will produce the following output:

```asm
          mask9to11:
7600 0077 		; Mask used later (also AND operation which has no effect)
          Start:
7601 4242 JMS	SysIO	; Start of program
7602 0005 		; 5 = write
7603 0373 		; Block to write
7604 7200 		; Write Address
7605 0000 		; Link
7606 7402 HLT		; Error
7607 4242 JMS	SysIO	; Write success - write next block
7610 0005 		; 5 = Write
7611 0374 		; Block to write
7612 7400 		; Write address
7613 0000 		; Link
7614 7402 HLT		; Error
7615 4242 JMS	SysIO	; Success
7616 0003 		; 3 = Read
7617 0001 		; Bock to read
7620 7200 		; Read Address
7621 0000 		; Link
7622 7402 HLT		; Error
7623 4242 JMS	SysIO	; Success
7624 0003 		; 3 = Read
7625 0002 		; Bock to read
7626 7400 		; Read Address
7627 0000 		; Link
7630 7402 HLT		; Error
7631 5626 JMP	I 7626	; Success - Indrect jump to OS start (7400)
7632 0006 AND	0006	
7633 0037 AND	0037	
7634 0040 AND	0040	
          mask6to8:
7635 0070 		; mask for bits 6-8
7636 3700 DCA	I 7700	
7637 2377 ISZ	7777	
7640 5360 JMP	7760	
7641 5303 JMP	7703	
          SysIO:
7642 0371 		; Return address stored here
7643 7240 CLA CMA		; Clear AC
7644 3377 DCA	7777	; Save zero?
7645 1642 TAD	I SysIO	; Get operation from caller
7646 3356 DCA	diskop	
7647 2242 ISZ	SysIO	; Set AC to 1
7650 1642 TAD	I SysIO	; AC = address of block to read parameter
7651 3352 DCA	block	; Store it
7652 2242 ISZ	SysIO	; Point to next parameter
7653 1642 TAD	I SysIO	; Get address
7654 1263 TAD	blockSize	
7655 3353 DCA	endData	
7656 1356 TAD	diskop	; Get disk operation
7657 7006 RTL		; Rotate left two
7660 7710 SPA CLA		; Skip next if ac > 0
7661 7120 STL		
7662 1356 TAD	diskop	
          blockSize:
7663 0200 AND	mask9to11	; 256
7664 3356 DCA	diskop	
7665 1356 TAD	diskop	
7666 0235 AND	mask6to8	
7667 1373 TAD	7773	
7670 3366 DCA	7766	
7671 2242 ISZ	SysIO	
7672 1242 TAD	SysIO	
7673 7430 SZL		
7674 7240 CLA CMA		
7675 3346 DCA	7746	
7676 4363 JMS	7763	
7677 2242 ISZ	SysIO	
7700 1642 TAD	I 7742	
7701 7430 SZL		
7702 3242 DCA	7742	
7703 7200 CLA		
7704 1352 TAD	10052	
7705 0233 AND	7733	
7706 7112 CLL RTR		
7707 7012 RTR		
7710 7012 RTR		
7711 1352 TAD	10052	
7712 7001 IAC		
7713 3363 DCA	10063	
7714 7430 SZL		
7715 1234 TAD	7734	
7716 1352 TAD	10052	
7717 7004 RAL		
7720 0236 AND	7736	
7721 1356 TAD	10056	
7722 6615 		
7723 7200 CLA		
7724 1356 TAD	10056	
7725 0232 AND	7732	
7726 1360 TAD	10060	
7727 3336 DCA	10036	
7730 1345 TAD	10045	
7731 3350 DCA	10050	
7732 1345 TAD	10045	
7733 1353 TAD	10053	
7734 3351 DCA	10051	
7735 1363 TAD	10063	
7736 6605 		
7737 6622 		
7740 5337 JMP	10037	
7741 6621 		
7742 5237 JMP	7737	
7743 2242 ISZ	7742	
7744 5360 JMP	10060	
7745 7577 		
7746 0370 AND	10070	
7747 0000 AND	0000	
7750 7750 		
7751 7751 		
          block:
7752 0000 AND	0000	
          endData:
7753 0000 		; End of data
7754 0000 AND	0000	
7755 0000 AND	0000	
          diskop:
7756 0005 		; Disk operation saved here
7757 0000 AND	0000	
7760 6601 		
7761 4363 JMS	10063	
7762 5642 JMP	I 7742	
7763 0001 AND	0001	
7764 1746 TAD	I 10046	
7765 3351 DCA	10051	
7766 6201 		
7767 1753 TAD	I 10053	
7770 3350 DCA	10050	
7771 1351 TAD	10051	
7772 3753 DCA	I 10053	
7773 6201 		
7774 1350 TAD	10050	
7775 3746 DCA	I 10046	
7776 5763 JMP	I 10063	
7777 7777 		
```
