package binfile

import (
	"io/ioutil"
)

type BinFile struct {
	pos  int
	data []byte
}

func Load(f string) (*BinFile, error) {
	var b BinFile

	data, err := ioutil.ReadFile(f)
	if err != nil {
		return nil, err
	}
	b.data = data
	b.pos = 0

	return &b, nil
}

func (b *BinFile) More() bool {
	return b.pos < len(b.data)
}

func (b *BinFile) NextWord() int16 {
	val := int16(b.data[b.pos]) + int16(b.data[b.pos+1])*256
	b.pos += 2
	return val & 0xfff
}
