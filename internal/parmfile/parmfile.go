package parmfile

import (
	"io/ioutil"
	"os"
	"strconv"

	yaml "gopkg.in/yaml.v2"
)

type ParmFile struct {
	Org      int64            `yaml:"org"`
	Skip     int64            `yaml:"skip-first"`
	Labels   map[int64]string `yaml:"labels"`
	Data     map[int64]string `yaml:"data"`
	Comments map[int64]string `yaml:"comments"`
}

func Load(f string) (*ParmFile, error) {
	var parms ParmFile

	file, err := os.Open(f)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(data, &parms)
	if err != nil {
		return nil, err
	}
	return &parms, nil
}

func readOctal(value string) (int64, error) {
	return strconv.ParseInt(value, 8, 16)
}
