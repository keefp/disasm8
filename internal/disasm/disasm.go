package disasm

import (
	"fmt"

	"gitlab.com/keefp/disasm8/internal/binfile"
	"gitlab.com/keefp/disasm8/internal/parmfile"
)

type instruction struct {
	addr      int16
	label     string
	field     int16
	opcode    string
	arguments string
	comment   string
}

var simpleInstrutions = [...]string{
	"AND",
	"TAD",
	"ISZ",
	"DCA",
	"JMS",
	"JMP",
}

var type7 = map[int16]string{
	07000: "NOP",
	07200: "CLA",
	07100: "CLL",
	07040: "CMA",     // 7040  complement AC                         1
	07202: "CML",     // 7020  complement link                       1
	07010: "RAR",     // 7010  rotate AC and link right one          2
	07004: "RAL",     // 7004  rotate AC and link left one           2
	07012: "RTR",     // 7012  rotate AC and link right two          2
	07006: "RTL",     // 7006  rotate AC and link left two           2
	07001: "IAC",     // 7001  increment AC
	07500: "SMA",     // 7500  skip on minus AC                      1
	07440: "SZA",     //   7440  skip on zero AC                       1
	07510: "SPA",     //   7510  skip on plus AC                       1
	07450: "SNA",     //   7450  skip on non zero AC                   1
	07420: "SNL",     //   7420  skip on non-zero link                 1
	07430: "SZL",     //   7430  skip on zero link                     1
	07410: "SKP",     //   7410  skip unconditionally                  1
	07404: "OSR",     //   7404  inclusive OR, switch register with AC 2
	07402: "HLT",     //   7402  halts the program                     1
	07600: "CLA",     //  7600  clear AC                              1
	07041: "CIA",     //     7041  complement and increment AC           1
	07604: "LAS",     //     7604  load AC with switch register          1
	07120: "STL",     //     7120  set link (to 1)                       1
	07204: "GLK",     //     7204  get link (put link in AC bit 11)      1
	07300: "CLA CLL", //  7300  clear AC and link                     1
	07201: "CLA IAC", //  7201  set AC = 1                            1
	07240: "CLA CMA", //  7240  set AC = -1                           1
	07110: "CLL RAR", //  7110  shift positive number one right       1
	07104: "CLL RAL", //  7104  shift positive number one left        1
	07106: "CLL RTL", //  7106  clear link, rotate 2 left             1
	07112: "CLL RTR", //  7112  clear link, rotate 2 right            1
	07640: "SZA CLA", //  7640  skip if AC = 0, then clear AC         1
	07460: "SZA SNL", //  7460  skip if AC = 0, or link is 1, or both 1
	07650: "SNA CLA", //  7650  skip if AC /= 0, then clear AC        1
	07700: "SMA CLA", //  7700  skip if AC < 0, then clear AC         1
	07540: "SMA SZA", //  7540  skip if AC <= 0                       1
	07520: "SMA SNL", //  7520  skip if AC < 0 or line is 1 or both   1
	07550: "SPA SNA", //  7550  skip if AC > 0                        1
	07530: "SPA SZL", //  7530  skip if AC >= 0 and if the link is 0  1
	07710: "SPA CLA", //  7710  skip of AC >= 0, then clear AC        1
	07470: "SNA SZL", //  7470  skip if AC /= 0 and link = 0          1
	07407: "DVI",     //      7407  divide                         <35
	07411: "NMI",     //      7411  normalize                      3.0+0.5n
	07413: "SHL",     //      7413  shift left                     3.0+0.5n
	07415: "ASR",     //      7415  arithmetic shift right         3.0+0.5n
	07417: "LSR",     //      7417  logical shift right            3.0+0.5n
	07421: "MQL",     //      7421  load AC into MQ, clear AC      1.5
	07405: "MUY",     //      7405  multiply                       9-21
	07501: "MQA",     //      7501  inclusive OR, MA with AC       1.5
	07621: "CAM",     //      7621  clear AC and MQ                1.5
	07441: "SCA",     //      7441  read SC into AC                1.5
}

var type6 = map[int16]string{
	06001: "ION", //      6001  turn interrupt on                     1
	06002: "IOF", //      6002  turn interrupt off                    1
	06004: "ADC", //      6004  convert A to D
	06031: "KSF", //      6031  skip if keyboard/reader flag = 1   2 1/2
	06032: "KCC", //      6032  clear AC and keyboard/reader       2 1/2
	06034: "KRS", //      6034  read keyboard/reader buffer,       2 1/2
	06036: "KRB", //      6036  clear AC, read keyboard buffer     2 1/2
	06041: "TSF", //      6041  skip if teleprinter/punch flag = 1 2 1/2
	06042: "TCF", //      6042  clear teleprinter/punch flag       2 1/2
	06044: "TPC", //      6044  load teleprinter/punch buffer, select and print
	06046: "TLS", //      6046  load teleprinter/punch buffer,     2 1/2
	//  select and print, and clear
	//  teleprinter/punch flag
	06011: "RSF",  //      6011  skip if reader flag = 1            2 1/2
	06012: "RRB",  //      6012  read reader buffer, and clear flag
	06014: "RFC",  //      6014  clear flag and buffer and fetch character
	06021: "PSF",  //      6021  skip if punch flag = 1             2 1/2
	06022: "PCF",  //      6022  clear flag and buffer              2 1/2
	06024: "PPC",  //      6024  load buffer and punch character
	06026: "PLS",  //      6026  clear flag and buffer; load and punch
	06053: "DXL",  //      6053  clear and load x buffer            2 1/2
	06063: "DYL",  //      6063  clear and load y buffer            2 1/2
	06057: "DXS",  //      6057  combined dxl and dix               2 1/2
	06067: "DYS",  //      6067  combined dyl and diy               2 1/2
	06064: "DIY",  //      6064  intensify point                    2 1/2
	06054: "DIX",  //      6054  intensify point                    2 1/2
	06061: "DCY",  //      6061  clear y buffer                     2 1/2
	06051: "DCX",  //      6051  clear x buffer                     2 1/2
	06761: "DTRA", //     6761  read status register A             1
	06762: "DTCA", //     6762  clear status register A            2
	06764: "DTXA", //     6764  load status register A             3
	06771: "DTSF", //     6771  skip on flags                      1
	06772: "DTRB", //     6772  read status register B             2
	06774: "DTLB", //     6774  load status register B             3
	//  CDF      62n1  change to data field n                1
	// 	CIF      62n2  change to instruction field n         1
	06214: "RDF", //      6214  read data field into AC 6-8           1
	06224: "RIF", //      6224  read instruction field into AC 6-8    1
	06244: "RMF", //      6244  restore memory field                  1
	06234: "RIB", //      6234  read interrupt buffer                 1
}

func Run(parms *parmfile.ParmFile, bin *binfile.BinFile) error {
	skip := parms.Skip
	loc := parms.Org
	for bin.More() {
		if skip > 0 {
			bin.NextWord()
			skip--
			continue
		}
		var inst instruction
		inst.addr = int16(loc)
		inst.field = bin.NextWord()
		if val, ok := parms.Data[loc]; !ok {
			if inst.field < 06000 {
				simpleInstruction(&inst, parms)
			} else if inst.field < 07000 {
				inst.opcode = type6[inst.field]
			} else {
				if inst.field&07707 == 06201 {
					changeField("CDF", &inst)
				} else if inst.field&07707 == 06201 {
					changeField("CIF", &inst)
				} else {
					inst.opcode = type7[inst.field]
				}
			}
		} else {
			inst.comment = val
		}
		checkLabel(&inst, parms)
		checkComment(&inst, parms)
		inst.print()
		loc++
	}
	return nil
}

func (i instruction) print() {
	if i.label != "" {
		fmt.Printf("          %s:\n", i.label)
	}
	fmt.Printf("%04o %04o %s\t%s\t", i.addr, i.field, i.opcode, i.arguments)
	if i.comment != "" {
		fmt.Printf("; %s\n", i.comment)
	} else {
		fmt.Println("")
	}
}

func changeField(op string, inst *instruction) {
	inst.opcode = op
	inst.arguments = fmt.Sprintf("%d", (inst.field&00070)>>3)
}

func simpleInstruction(inst *instruction, parm *parmfile.ParmFile) {
	inst.opcode = simpleInstrutions[(inst.field&07000)>>9]
	i := inst.field & 00400
	z := inst.field & 00200
	offset := inst.field & 00177
	if z != 0 {
		offset += (inst.addr & 07700)
	}
	args := ""
	offsetStr := ""
	if i > 0 {
		args += "I "
	}
	if val, ok := parm.Labels[int64(offset)]; ok {
		offsetStr = val
	} else {
		offsetStr = fmt.Sprintf("%04o", offset)
	}
	inst.arguments = args + offsetStr
}

func checkComment(i *instruction, parms *parmfile.ParmFile) {
	if val, ok := parms.Comments[int64(i.addr)]; ok {
		i.comment = val
	}
}

func checkLabel(i *instruction, parms *parmfile.ParmFile) {
	if val, ok := parms.Labels[int64(i.addr)]; ok {
		i.label = val
	}
}
