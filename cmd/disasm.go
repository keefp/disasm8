package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/keefp/disasm8/internal/binfile"
	"gitlab.com/keefp/disasm8/internal/disasm"
	"gitlab.com/keefp/disasm8/internal/parmfile"
)

func init() {
	flag.StringVar(&binaryFile, "binary", "-", "Input binary file - for stdin (default)")
	flag.StringVar(&parmFile, "parm", "", "Parameter file describing binary (no default)")
	flag.Parse()
}

var binaryFile string
var parmFile string

func main() {
	parms, err := parmfile.Load(parmFile)
	if err != nil {
		fmt.Printf("Unable to load parm file %s %v\n", parmFile, err)
		os.Exit(1)
	}
	bin, err := binfile.Load(binaryFile)
	if err != nil {
		fmt.Printf("Unable to open binary file %v\n", err)
		os.Exit(1)
	}
	disasm.Run(parms, bin)
}
